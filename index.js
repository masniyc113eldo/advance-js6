const findButton = document.querySelector('#findIP');
findButton.addEventListener('click', async () => {
    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        
        const geoResponse = await fetch(`https://ip-api.com/json/${ipAddress}`);

        const resultDiv = document.querySelector('#result');
        resultDiv.innerHTML = `
            <p>Континент: ${geoData.continent}</p>

            <p>Країна: ${geoData.country}</p>
            
            <p>Регіон: ${geoData.regionName}</p>

            <p>Місто: ${geoData.city}</p>
            
            <p>Район: ${geoData.district}</p>
        `;
    } catch (error) {
        console.error('Помилка:', error);
    }
});
